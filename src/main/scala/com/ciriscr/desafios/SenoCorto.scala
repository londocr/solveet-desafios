package com.ciriscr.desafios

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 29/10/12
 * Time: 10:53 PM
 */

trait SenoCorto {

  def senoCorto(s: String) = {
    import math._
    val resumen = s.groupBy(x => x).map(y => (y._1, y._2.size))
    val h = resumen.getOrElse('N', 0) - resumen.getOrElse('S', 0)
    val v = resumen.getOrElse('E', 0) - resumen.getOrElse('O', 0)
    ((if (h >= 0) "N" else "S") * abs(h)) + ((if (v >= 0) "E" else "O") * abs(v))
  }
}
