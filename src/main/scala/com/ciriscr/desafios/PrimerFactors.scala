package com.ciriscr.desafios

import collection.mutable.ListBuffer

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 25/07/12
 * Time: 11:42 AM
 */

//código fuente; https://bitbucket.org/londo/solveet-desafios
trait PrimerFactors extends NumerosPrimos{

  val primes = ListBuffer.empty[Int]

  def getFactors(n: Int): List[Int] = {
    fullPrimes(n)
    val lista = ListBuffer.empty[Int]
    var res = n
    val tam = primes.length
    var i = 0
    while (i < tam && primes.find(_ == res).isEmpty){
      if (res % primes(i) == 0) {
        lista += primes(i)
        res /= primes(i)
      } else i += 1
    }
    lista += res
    lista.toList
  }

  private def fullPrimes(n: Int) {
    val max = if (primes.isEmpty) 2 else primes.maxBy(i => i)
    if (max < n)
      for (c <- max to n; if (esPrimo(c))) primes += c
  }

}
