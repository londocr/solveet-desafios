package com.ciriscr.desafios

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 01/06/12
 * Time: 12:33 PM
 */

//código fuente; https://bitbucket.org/londo/solveet-desafios
trait PrimosGemelos extends NumerosPrimos{

  def sonGemelos(n: Int, m: Int) = math.abs(n - m) == 2 && esPrimo(n) && esPrimo(m)

}

trait NumerosPrimos {

  protected def esPrimo(n: Int) = {
    if (n == 2 || n == 3 || n == 5 || n == 7) true
    else {
      if (n > 1 && n % 2 != 0 && n % 3 != 0 && n % 5 != 0 && n % 7 != 0) {          //se pueden meter más validaciones para evitar tiempo de procesamiento
      var i = 11
        var seguir = true
        while (i * i <= n && seguir) {
          seguir = n % i != 0
          i += 1
        }
        seguir
      } else false
    }
  }

}
