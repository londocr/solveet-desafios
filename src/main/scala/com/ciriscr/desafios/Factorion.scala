package com.ciriscr.desafios

import scala.collection.mutable.ArrayBuffer

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 23/07/12
 * Time: 08:24 PM
 */

//código fuente; https://bitbucket.org/londo/solveet-desafios
trait Factorion {

  val factoriales = Map("0" -> factorial(0), "1" -> factorial(1), "2" -> factorial(2), "3" -> factorial(3),
                        "4" -> factorial(4), "5" -> factorial(5), "6" -> factorial(6), "7" -> factorial(7),
                        "8" -> factorial(8), "9" -> factorial(9))

  private def factorial(n: Int): Int = if (n == 0) 1 else n * factorial(n-1)

  def esFactorion(n: Int) = n.toString.split("").map(s => factoriales.getOrElse(s, 0)).fold(0)(_ + _) == n

}

trait RunFactorion extends Factorion{
  def procesar = for (n <- 0 to 2500000; if esFactorion(n)) println(n)
}
