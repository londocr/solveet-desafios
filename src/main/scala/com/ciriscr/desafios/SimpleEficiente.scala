package com.ciriscr.desafios

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 19/07/12
 * Time: 09:19 PM
 */

//código fuente; https://bitbucket.org/londo/solveet-desafios
trait SimpleEficiente {

  def f(x: BigInt, y: BigInt, z: BigInt): BigInt = {
    if (x <= y) y
    else {
      lazy val _x = f(x-1,y,z)
      lazy val _y = f(y-1,z,x)
      lazy val _z = f(z-1,x,y)
      f(_x, _y, _z)
    }
  }

  def f2(x: BigInt, y: BigInt, z: BigInt): BigInt = {
    if (x <= y) y
    else
      f(f(x-1,y,z), f(y-1,z,x), f(z-1,x,y))
  }

}
