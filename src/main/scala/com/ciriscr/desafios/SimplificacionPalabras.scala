package com.ciriscr.desafios

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 30/10/12
 * Time: 12:11 AM
 */

trait SimplificacionPalabras {

  def simpleWord(s: String): Option[String] = {
    val texto = s.toUpperCase.map(parseLetter)
    if (texto.matches("[A-Z]*")) Some(texto)
    else None
  }

  def parseLetter(c: Char) = {
    c match {
      case 'Á' | 'Ä' | 'À' | 'Â' => 'A'
      case 'É' | 'Ë' | 'È' | 'Ê' => 'E'
      case 'Í' | 'Ï' | 'Ì' | 'Î' => 'I'
      case 'Ó' | 'Ö' | 'Ò' | 'Ô' => 'O'
      case 'Ú' | 'Ü' | 'Ù' | 'Û' => 'U'
      case 'Ñ' => 'N'
      case w => w
    }
  }

}
