package com.ciriscr.desafios

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 31/05/12
 * Time: 09:22 PM
 */

//código fuente; https://bitbucket.org/londo/solveet-desafios
trait MinaDiamantes {

  def extraerDiamantes(mina: String) = {
    var s = mina
    while (s.contains("<>")){
      s = s.replaceAllLiterally("<>", "")
    }
    (mina.length - s.length) / 2
  }

}
