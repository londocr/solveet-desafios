package com.ciriscr.desafios

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 13/11/12
 * Time: 11:57 AM
 */

//código fuente; https://bitbucket.org/londo/solveet-desafios
trait CryptoCesar {

  val encriptar = procesar(false)_
  val desencriptar = procesar(true)_

  private def procesar(isForward: Boolean)(text: String, n: Int): String = {
    val k = key(n)(isForward)
    text.toLowerCase.map(s => k.getOrElse(s.toString, s) ).mkString
  }

  private def key(n: Int)(isDecrypt: Boolean = false):Map[String, String] = {
    val let = List("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
      "u", "v", "w", "x", "y", "z")
    val des = let.drop(n) ::: let.take(n)
    val zipK = if (isDecrypt) des.zip(let) else let.zip(des)
    Map(zipK:_*)
  }

}