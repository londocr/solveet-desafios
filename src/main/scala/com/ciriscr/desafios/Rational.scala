package com.ciriscr.desafios

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 19/07/12
 * Time: 09:43 PM
 */

// código fuente; https://bitbucket.org/londo/solveet-desafios
//
// Este código no es totalmente mio, lo copie del libro "Programming in Scala" de Martin Odersky, Lex Spoon
// y Bill Venners. Yo lo amplie/modifique en:
//    * Usar Long
//    * Colocar los metodos sobrecargados de Byte, Short y Long
//    * toString, toDouble, isNegative, isPositive, normalize y unary_-
//    * Comparaciones booleanas
class Rational(val n:Long, val d:Long = 1) {
  require(d != 0)     //si es menor que cero el sistema tira un IllegalArgumentException

  override def toString = (if (isNegative) "-" else "") + n.abs.toLong + "/" + d.abs.toLong
  def toDouble = n/(d * 1.0)
  def isNegative = (math.signum(n) * math.signum(d)) < 0
  def isPositive = !isNegative
  def normalize = {
    val g = gcd(n, d)
    new Rational(n/g, d/g)
  }
  def unary_- = *(-1)

  def +(r: Rational) = new Rational(n * r.d + r.n * d, d * r.d)
  def +(r: Byte): Rational = this + r.toLong
  def +(r: Short): Rational = this + r.toLong
  def +(r: Int): Rational = this + r.toLong
  def +(r: Long): Rational = this + new Rational(r)

  def -(r: Rational) = new Rational(n * r.d - r.n * d, d * r.d)
  def -(r: Byte): Rational = this - r.toLong
  def -(r: Short): Rational = this - r.toLong
  def -(r: Int): Rational = this - r.toLong
  def -(r: Long): Rational = this - new Rational(r)

  def *(r: Rational) = new Rational(n * r.n, d * r.d)
  def *(r: Byte): Rational = *(r.toLong)
  def *(r: Short): Rational = *(r.toLong)
  def *(r: Int): Rational = *(r.toLong)
  def *(r: Long): Rational = this * new Rational(r)

  def /(r: Rational) = this * new Rational(r.d, r.n)
  def /(r: Byte): Rational = /(r.toLong)
  def /(r: Short): Rational = /(r.toLong)
  def /(r: Int): Rational = /(r.toLong)
  def /(r: Long): Rational = this / new Rational(r)

  private def compare(r: Rational): Double = (this - r).toDouble

  def <(r: Rational): Boolean = (this compare r) <  0
  def <(r: Byte): Boolean = <(r.toLong)
  def <(r: Short): Boolean = <(r.toLong)
  def <(r: Int): Boolean = <(r.toLong)
  def <(r: Long): Boolean = this < new Rational(r)

  def >(r: Rational): Boolean = (this compare r) >  0
  def >(r: Byte): Boolean = >(r.toLong)
  def >(r: Short): Boolean = >(r.toLong)
  def >(r: Int): Boolean = >(r.toLong)
  def >(r: Long): Boolean = this > new Rational(r)

  def <=(r: Rational): Boolean = (this compare r) <= 0
  def <=(r: Byte): Boolean = <=(r.toLong)
  def <=(r: Short): Boolean = <=(r.toLong)
  def <=(r: Int): Boolean = <=(r.toLong)
  def <=(r: Long): Boolean = this <= new Rational(r)

  def >=(r: Rational): Boolean = (this compare r) >= 0
  def >=(r: Byte): Boolean = >=(r.toLong)
  def >=(r: Short): Boolean = >=(r.toLong)
  def >=(r: Int): Boolean = >=(r.toLong)
  def >=(r: Long): Boolean = this >= new Rational(r)

  def ==(r: Rational): Boolean = (this compare r) == 0
  def ==(r: Byte): Boolean = ==(r.toLong)
  def ==(r: Short): Boolean = ==(r.toLong)
  def ==(r: Int): Boolean = ==(r.toLong)
  def ==(r: Long): Boolean = this == new Rational(r)

  def !=(r: Rational): Boolean = !(==(r))
  def !=(r: Byte): Boolean = !=(r.toLong)
  def !=(r: Short): Boolean = !=(r.toLong)
  def !=(r: Int): Boolean = !=(r.toLong)
  def !=(r: Long): Boolean = this != new Rational(r)

  def max(r: Rational) = if (this < r) r else this
  def min(r: Rational) = if (this > r) r else this
  private def gcd(a: Long, b: Long): Long = if (b == 0) a else gcd(b, a % b)

}
