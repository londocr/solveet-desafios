package com.ciriscr.desafios

import org.scalatest.FunSuite

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 30/10/12
 * Time: 12:20 AM
 */

class TestSimplificacionPalabras extends FunSuite with SimplificacionPalabras{

  test("simplificacion") {
    assert(simpleWord("Cigüeña") === Some("CIGUENA"))
    assert(simpleWord("Caribú rojo") === None)
  }

}
