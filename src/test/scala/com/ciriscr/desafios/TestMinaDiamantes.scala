package com.ciriscr.desafios

import org.scalatest.FunSuite

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 31/05/12
 * Time: 08:25 PM
 */

class TestMinaDiamantes extends FunSuite with MinaDiamantes {

  test("MinaDiamantes") {
    assert(extraerDiamantes("") === 0)
    assert(extraerDiamantes("<>") === 1)
    assert(extraerDiamantes("<<>>") === 2)
    assert(extraerDiamantes("<><<>><<") === 3)
  }
}
