package com.ciriscr.desafios

import org.scalatest.FunSuite

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 29/10/12
 * Time: 11:03 PM
 */

class TestSenoCorto extends FunSuite with SenoCorto {

  test("seno corto") {
    assert(senoCorto("SENO") === "")
    assert(senoCorto("SSEEO") === "SSE")
    assert(senoCorto("SEEESOONNO") === "")
  }

}
