package com.ciriscr.desafios

import org.scalatest.FunSuite

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 13/11/12
 * Time: 12:25 PM
 */

class TestCryptoCesar extends FunSuite with CryptoCesar {

  test("CryptoCesar") {
    val text = "Todo lo que se preguntaba eran las mismas respuestas que buscamos el resto de nosotros. ¿De dónde vengo? ¿A dónde voy? ¿Cuánto tiempo tengo? Todo lo que pude hacer fue sentarme y ver como moría.".toLowerCase
    val msg = "wrgr or txh vh suhjxqwded hudq odv plvpdv uhvsxhvwdv txh exvfdprv ho uhvwr gh qrvrwurv. ¿gh góqgh yhqjr? ¿d góqgh yrb? ¿fxáqwr wlhpsr whqjr? wrgr or txh sxgh kdfhu ixh vhqwduph b yhu frpr pruíd."

    assert(msg === encriptar(text, 3))
    assert(text === desencriptar(encriptar(text, 3), 3))
    assert(text === desencriptar(msg, 3))
  }

}
