package com.ciriscr.desafios

import org.scalatest.FunSuite

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 01/06/12
 * Time: 09:40 PM
 */

class TestLagartoSpock extends FunSuite with LagartoSpock {

  test("Piedra-Papel-Tijera-Lagarto-Spock") {
    assert(validarJuego("Tijeras" -> "Tijeras") === Resultado.Empate)

    assert(validarJuego("Spock" -> "Tijeras") === Resultado.Gana1)
    assert(validarJuego("Spock" -> "Papel") === Resultado.Gana2)
    assert(validarJuego("Tijeras" -> "Lagarto") === Resultado.Gana1)
    assert(validarJuego("Tijeras" -> "Spock") === Resultado.Gana2)
    assert(validarJuego("Papel" -> "Piedra") === Resultado.Gana1)
    assert(validarJuego("Papel" -> "Tijeras") === Resultado.Gana2)
    assert(validarJuego("Piedra" -> "Lagarto") === Resultado.Gana1)
    assert(validarJuego("Piedra" -> "Papel") === Resultado.Gana2)
    assert(validarJuego("Lagarto" -> "Spock") === Resultado.Gana1)
    assert(validarJuego("Lagarto" -> "Piedra") === Resultado.Gana2)
  }

}
