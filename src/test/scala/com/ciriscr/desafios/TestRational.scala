package com.ciriscr.desafios

import org.scalatest.FunSuite

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 01/06/12
 * Time: 09:40 PM
 */

class TestRational extends FunSuite {

  val r = new Rational(1, 2)

  test("Racionales basicas") {
    assert(r.toString === "1/2")
    assert(r.toDouble === 0.5)
    assert((-r).toString === "-1/2")
    assert((-r).isNegative)
    assert(r.isPositive)
    assert(r.toString === new Rational(2, 4).normalize.toString)
    assert(r.max(new Rational(1,3)).toString === "1/2")
    assert(r.min(new Rational(2,3)).toString === "1/2")
  }

  test("Racionales sumas") {
    assert((r + new Rational(1, 2)).toString === "4/4")
    assert((r + 1.toByte).toString === "3/2")
    assert((r + 1.toShort).toString === "3/2")
    assert((r + 1).toString === "3/2")
    assert((r + 1.toLong).toString === "3/2")
  }

  test("Racionales restas") {
    assert((r - new Rational(1, 2)).toString === "0/4")
    assert((r - 1.toByte).toString === "-1/2")
    assert((r - 1.toShort).toString === "-1/2")
    assert((r - 1).toString === "-1/2")
    assert((r - 1.toLong).toString === "-1/2")
  }

  test("Racionales multiplicaciones") {
    assert((r * new Rational(1, 2)).toString === "1/4")
    assert((r * 3.toByte).toString === "3/2")
    assert((r * 3.toShort).toString === "3/2")
    assert((r * 3).toString === "3/2")
    assert((r * 3.toLong).toString === "3/2")
  }

  test("Racionales divisiones") {
    assert((r / new Rational(2, 2)).toString === "2/4")
    assert((r / 3.toByte).toString === "1/6")
    assert((r / 3.toShort).toString === "1/6")
    assert((r / 3).toString === "1/6")
    assert((r / 3.toLong).toString === "1/6")
  }

  test("Racionales menor que") {
    assert(r < new Rational(3, 2))
    assert(r < 1.toByte)
    assert(r < 1.toShort)
    assert(r < 1)
    assert(r < 1.toLong)
  }

  test("Racionales menor igual que") {
    assert(r <= new Rational(1, 2))
    assert(r <= 1.toByte)
    assert(r <= 1.toShort)
    assert(r <= 1)
    assert(r <= 1.toLong)
  }

  test("Racionales mayor que") {
    assert(r > new Rational(0, 2))
    assert(r > 0.toByte)
    assert(r > 0.toShort)
    assert(r > 0)
    assert(r > 0.toLong)
  }

  test("Racionales mayor igual que") {
    assert(r >= new Rational(1, 2))
    assert(r >= 0.toByte)
    assert(r >= 0.toShort)
    assert(r >= 0)
    assert(r >= 0.toLong)
  }

  test("Racionales igual que") {
    assert(r == new Rational(1, 2))
    assert((r+r) == 1.toByte)
    assert((r+r) == 1.toShort)
    assert((r+r) == 1)
    assert((r+r) == 1.toLong)
  }

  test("Racionales diferente que") {
    assert(r != new Rational(2, 2))
    assert(r != 1.toByte)
    assert(r != 1.toShort)
    assert(r != 1)
    assert(r != 1.toLong)
  }

}
