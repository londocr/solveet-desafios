package com.ciriscr.desafios

import org.scalatest.FunSuite

/**
  * Created with IntelliJ IDEA.
  * User: londo
  * Date: 23/07/12
  * Time: 08:43 PM
  */

class TestBrainfuckInterpreter extends FunSuite with BrainfuckInterpreter {

  test("brainfuck") {
    interpretar("++++++++++[>+++++++>++++++++++>+++>+<<<<-]>++.>+.+++++++..+++.>++.<<+++++++++++++++.>.+++.------.--------.>+.>.".toCharArray)
  }

 }
