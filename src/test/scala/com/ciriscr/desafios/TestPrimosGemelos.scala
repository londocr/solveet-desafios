package com.ciriscr.desafios

import org.scalatest.FunSuite

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 01/06/12
 * Time: 02:03 PM
 */

class TestPrimosGemelos extends FunSuite with PrimosGemelos {

  test("Primos Gemelos"){
    assert(sonGemelos(2,3) === false)
    assert(sonGemelos(5,7) === true)
    assert(sonGemelos(29,31) === true)
    assert(sonGemelos(9999971,9999973) === true)
    assert(sonGemelos(9999971,9999974) === false)
    assert(sonGemelos(21,23) === false)
  }

}